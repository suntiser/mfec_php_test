(function($){
     $(window).on("load",function(){
        $(".fixed-content-container").mCustomScrollbar({
             theme:"dark",
             axis:"y",
        });
    });
})(jQuery);

function loading_on(){
	$('.loading-page').css('display','block');
	$(this).delay(10).queue(function() {
		$('.loading-page').addClass('active');
    	$(this).dequeue();
  });
}
function loading_off(){
	$('.loading-page').removeClass('active');
	$(this).delay(600).queue(function() {
		$('.loading-page').css('display','none');
    	$(this).dequeue();
  });
}
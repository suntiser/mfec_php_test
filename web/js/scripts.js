function checkemail(emailAddress) {
	
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	
	return pattern.test(emailAddress);
	
};

jQuery(document).ready(function($){
    $('.mash-menu').mashableMenu({
        separator                      : false,
        ripple_effect                  : false,
        search_bar_hide                : true,
        top_fixed                      : false,
        full_width                     : false,
        right_to_left                  : false,
        trigger                        : 'hover',
        vertical_tabs_trigger          : 'click',
        vertical_tabs_effect_speed     : 400,
        drop_down_effect_in_speed      : 200,
        drop_down_effect_out_speed     : 200,
        drop_down_effect_in_delay      : 200,
        drop_down_effect_out_delay     : 200,
        outside_close_dropDown         : true,
        sticky_header                  : false,
        sticky_header_height           : 200,
        sticky_header_animation_speed  : 400,
        timer_on                       : false,
        timer_morning_color            : 'cyan',
        timer_afternoon_color          : 'red',
        timer_evening_color            : 'teal',
        internal_links_enable          : true,
        internal_links_toggle_drop_down: false,
        internal_links_target_speed    : 400,
        mobile_search_bar_hide         : false,
        mobile_sticky_header           : false,
        mobile_sticky_header_height    : 100,
        media_query_max_width          : 768
    });
    
    $('.mash-menu .mash-list-items > li.active .menu-active').css('left',($('.mash-menu .mash-list-items > li.active').innerWidth() / 2) - 10).slideDown();
    
});

$(window).resize(function() {
    $('.mash-menu .mash-list-items > li.active .menu-active').css('left',($('.mash-menu .mash-list-items > li.active').innerWidth() / 2) - 10);
});
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title><?php include_title()?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="fonts/stylesheet.css" />
    <link rel="stylesheet" href="css/font-awesome.css" />
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <link href="css/style-map.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <?php include 'inc/_ie.php'; ?>
</head>

  <body class="login-body">

    <div class="container">
        <header class="header white-bg">
          <!--logo start-->
          <a href="index.html" class="logo"><img src="images/logo-map.png" alt="" /></a>
          <!--logo end-->
      </header>
      <form class="form-signin" action="home.html">
        <h2 class="form-signin-heading">WELCOME, please Log in</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="User ID" autofocus>
            <input type="password" class="form-control" placeholder="Password">
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <a class="btn btn-lg btn-login btn-block" href="home.php">Login</a>

        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" type="button">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->
          <?php include 'inc/_modal.php'; ?>

      </form>

    </div>
<div class="login-footer">
    <div class="login-footer-area">
        <div class="box-logo">
            <div class="img"><img src="images/Boss.png" alt="" /></div>
            <div class="img"><img src="images/Loreal.png" alt="" /></div>
            <div class="img"><img src="images/Adecco.png" alt="" /></div>
        </div>
    </div>
</div>


    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>


  </body>
</html>

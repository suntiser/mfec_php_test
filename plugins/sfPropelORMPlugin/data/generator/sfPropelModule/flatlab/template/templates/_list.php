<div class="col-lg-9">
    <section class="panel">
      <section class="panel-heading">[?php echo <?php echo $this->getI18NString('list.title') ?> ?]</section>
      [?php if (!$pager->getNbResults()): ?]
        <p>[?php echo __('No result', array(), 'sf_admin') ?]</p>
      [?php else: ?]
        <table cellspacing="0" class="table table-striped table-advance table-hover">
          <thead>
            <tr>
    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
              <?php /*<th id="sf_admin_list_batch_actions" style="width: 50px; text-align: center;"><input id="sf_admin_list_batch_checkbox" type="checkbox" onclick="checkAll();" /></th>*/?>
    <?php endif; ?>
              [?php include_partial('<?php echo $this->getModuleName() ?>/list_th_<?php echo $this->configuration->getValue('list.layout') ?>', array('sort' => $sort)) ?]
    <?php if ($this->configuration->getValue('list.object_actions')): ?>
              <th style="width: 100px;"><i class="fa fa-edit"></i>[?php echo __('Actions', array(), 'sf_admin') ?]</th>
    <?php endif; ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th colspan="<?php echo count($this->configuration->getValue('list.display')) + ($this->configuration->getValue('list.object_actions') ? 1 : 0) + ($this->configuration->getValue('list.batch_actions') ? 1 : 0) ?>">
                [?php if ($pager->haveToPaginate()): ?]
                  [?php include_partial('<?php echo $this->getModuleName() ?>/pagination', array('pager' => $pager)) ?]
                [?php endif; ?]
    
                [?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?]
                [?php if ($pager->haveToPaginate()): ?]
                  [?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?]
                [?php endif; ?]
              </th>
            </tr>
          </tfoot>
          <tbody>
            [?php foreach ($pager->getResults() as $i => $<?php echo $this->getSingularName()?>): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?]
              <tr>
    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
                [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_batch_actions', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'helper' => $helper)) ?]
    <?php endif; ?>
                [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_<?php echo $this->configuration->getValue('list.layout') ?>', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>)) ?]
    <?php if ($this->configuration->getValue('list.object_actions')): ?>
                [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_actions', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'helper' => $helper)) ?]
    <?php endif; ?>
              </tr>
            [?php endforeach; ?]
          </tbody>
        </table>
      [?php endif; ?]
    </section>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>

<td style="text-align: center;">
  <ul style="list-style: none; padding: 0; margin: 0;">
<?php foreach ($this->configuration->getValue('list.object_actions') as $name => $params): ?>
<?php if ('_delete' == $name): ?>
    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToDelete($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

<?php elseif ('_edit' == $name): ?>
    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToEdit($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

<?php elseif ('_move_up' == $name): ?>
    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToMoveUp($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

<?php elseif ('_move_down' == $name): ?>
    <?php echo $this->addCredentialCondition('[?php echo $helper->linkToMoveDown($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

<?php else: ?>
<?php /*
    <li class="sf_admin_action_<?php echo $params['class_suffix'] ?>">*/?>
      <?php echo $this->addCredentialCondition($this->getLinkToAction($name, $params, true), $params) ?>

<?php /*    </li>*/?>
<?php endif; ?>
<?php endforeach; ?>
  </ul>
</td>

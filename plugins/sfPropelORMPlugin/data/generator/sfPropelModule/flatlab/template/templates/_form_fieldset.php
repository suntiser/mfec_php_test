<div class="row">
    <div class="col-lg-9">
        <section class="panel">
<?php /*<fieldset id="sf_fieldset_[?php echo preg_replace('/[^a-z0-9_]/', '_', strtolower($fieldset)) ?]">*/?>
  [?php if ('NONE' != $fieldset): ?]
    <?php /*<h2>[?php echo __($fieldset, array(), '<?php echo $this->getI18nCatalogue() ?>') ?]</h2>*/?>
            <header class="panel-heading">[?php echo __($fieldset, array(), '<?php echo $this->getI18nCatalogue() ?>') ?]</header>
  [?php endif; ?]
            <div class="panel-body">
  [?php foreach ($fields as $name => $field): ?]
    [?php if ((isset($form[$name]) && $form[$name]->isHidden()) || (!isset($form[$name]) && $field->isReal())) continue ?]
    [?php include_partial('<?php echo $this->getModuleName() ?>/form_field', array(
      'name'       => $name,
      'attributes' => $field->getConfig('attributes', array()),
      'label'      => $field->getConfig('label'),
      'help'       => $field->getConfig('help'),
      'form'       => $form,
      'field'      => $field,
      'class'      => 'form-group sf_admin_'.strtolower($field->getType()).' sf_admin_form_field_'.$name,
    )) ?]
  [?php endforeach; ?]
            </div>
<?php /*</fieldset>*/?>
        </section>
    </div>
</div>
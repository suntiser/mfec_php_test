[?php use_stylesheets_for_form($form) ?]
[?php use_javascripts_for_form($form) ?]

<?php /*<div class="col-lg-9">*/?>
  [?php echo form_tag_for($form, '@<?php echo $this->params['route_prefix'] ?>') ?]
    [?php echo $form->renderHiddenFields(false) ?]

    [?php if ($form->hasGlobalErrors()): ?]
      [?php //echo $form->renderGlobalErrors() ?]
    [?php endif; ?]

    [?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?]
      [?php include_partial('<?php echo $this->getModuleName() ?>/form_fieldset', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?]
    [?php endforeach; ?]

    [?php include_partial('<?php echo $this->getModuleName() ?>/form_actions', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?]
  </form>
<?php /*</div>*/?>
[?php $errors = $form->getErrorSchema()->getErrors()?]
  [?php if( $errors ):?]
<script type="text/javascript">
[?php foreach ($errors as $name => $error):?]
    var parent = $('#[?php echo "<?php echo $this->getModuleName() ?>_".preg_replace('/\[|\]/', '_', substr($name, 0, strlen($name) - 1))?]').parent();
    parent.addClass('has-error');
    parent.append('<p class="help-block">[?php echo $error?]</p>');
    
[?php endforeach;?]
</script>
[?php endif;?]


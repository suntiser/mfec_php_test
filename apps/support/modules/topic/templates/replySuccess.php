<?php $topic = isset($topic)? $topic: new Topic()?>
<table width="600px">
    <tr>
        <td><b><?php echo htmlspecialchars($topic->getTitle())?></b></td>
    </tr>
    <tr>
        <td>
            <?php echo htmlspecialchars($topic->getMessage())?><br/><br/>
            <b>By <?php echo $topic->getCreatedBy()?></b> <i><?php echo $topic->getCreatedAt()?></i>
        </td>
    </tr>
</table>

<?php if(isset($replys) && count($replys) > 0):?>
    <table width="600px">
        <?php foreach($replys as $i => $reply):?>
            <tr>
                <td>
                    <?php echo htmlspecialchars($reply->getMessage())?><br/>
                    <b>By <?php $reply->getCreatedBy()?></b> <i><?php echo $reply->getCreatedAt()?></i>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
<?php endif;?>

<br/>
<form action="<?php echo url_for('@postReply')?>" method="post">
    <input type="hidden" name="id" value="<?php echo $topic->getId()?>" />
    <table>
        <tr>
            <td>
                Reply<br/>
                <textarea rows="5" name="message"></textarea>
                <?php if(isset($errors['message'])):?>
                    <div class="error-text"><?php echo $errors['message']?></div>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </table>
</form>
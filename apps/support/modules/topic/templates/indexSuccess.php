<table width="600px">
    <tr>
        <th>#</th>
        <th>Subject</th>
    </tr>
    <?php foreach ($topics as $i => $topic):?>
        <tr>
            <td><?php echo $i + 1?></td>
            <td>
                <a href="<?php echo url_for("@reply?id={$topic->getId()}")?>"><?php echo htmlspecialchars($topic->getTitle())?></a>
                <?php if(Role::allowDelete($sf_user->getAttribute('role'))):?>
                    [<a href="<?php echo url_for("@delete?id={$topic->getId()}")?>">ลบกระทู้</a>]
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
<form action="<?php echo url_for('topic/post')?>" method="post">
    <table>
        <tr>
            <td>
                Title<br/>
                <input type="text" name="title" />
                <?php if(isset($errors['title'])):?>
                    <div class="error-text"><?php echo $errors['title']?></div>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <td>
                Message<br/>
                <textarea rows="5" name="message"></textarea>
                <?php if(isset($errors['message'])):?>
                    <div class="error-text"><?php echo $errors['message']?></div>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </table>
</form>
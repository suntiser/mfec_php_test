<?php
class topicActions extends sfActions {
    /**
     * Executes index action
     *
     * @param sfRequest $request
     *            A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->initTopics();
    }
    
    public function executePost(sfWebRequest $request) {
        $this->initTopics();
        if(sfRequest::POST == $request->getMethod()) {
            $topic = null;
            if($id = trim($request->getParameter('id'))) {
                $topic = TopicQuery::create()
                    ->filterById(intval($id))
                    ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
                    ->findOne();
                $topic
                    ->setAuditFlag('U')
                    ->setUpdatedAt(time())
                    ->setUpdatedById($this->getLoginId());
            }
            else {
                $topic = new Topic();
                $topic
                    ->setCreatedById($this->getLoginId())
                    ->setCreatedAt(time())
                    ->setAuditFlag('C');
            }
            
            $this->bindTopic($topic, $request);
            if($errors = $this->validatePost($topic)) {
                $this->errors = $errors;
            }
            else {
                $topic->save();
                $this->redirect('@topic');
            }
        }
        
        $this->setTemplate('index');
    }
    
    private function initTopics() {
        $this->topics = TopicQuery::create()
            ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
            ->orderByCreatedAt(Criteria::DESC)
            ->find();
    }
    
    private function validatePost(Topic $topic) {
        $errors = array();
        if(!$topic->getTitle()) {
            $errors['title'] = 'This field is required!';
        }
        
        if(!$topic->getMessage()) {
            $errors['message'] = 'This field is required!';
        }
        
        return $errors;
    }
    
    public function executeReply(sfWebRequest $request) {
        if($id = trim($request->getParameter('id'))) {
            $this->topic = TopicQuery::create()
                ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
                ->filterById(intval($id))
                ->findOne();
            
            if(sfRequest::POST == $request->getMethod()) {
                $reply = new Reply();
                $reply
                    ->setMessage(trim($request->getParameter('message')))
                    ->setTopicId(intval($id))
                    ->setCreatedById($this->getLoginId())
                    ->setCreatedAt(time())
                    ->save();
            }
        }
    }
    
    public function executeDelete(sfWebRequest $request) {
        $roles = $this->getUser()->getAttribute('roles');
        if(!Role::allowDelete($roles)) {
            die("No permission to delete.");
        }
        
        if($id = trim($request->getParameter('id'))) {
            TopicQuery::create()
                ->filterById(intval($id))
                ->update(array('AuditFlag' => 'D', 'UpdatedAt' => time(), 'UpdatedById' => $this->getLoginId()));
        }
    }
    
    private function bindTopic(Topic &$topic, sfWebRequest $request ) {
        $names = $request->getParameterHolder()->getNames();
        foreach($names as $name) {
            $methodName = 'set' . ucfirst($name);
            if(method_exists($topic, $methodName)) {
                call_user_func_array(array($topic, $methodName), array(trim($request->getParameter($name))));
            }
        }
    }
    
    private function bindReply(Reply &$reply, sfWebRequest $request ) {
        $names = $request->getParameterHolder()->getNames();
        foreach($names as $name) {
            $methodName = 'set' . ucfirst($name);
            if(method_exists($topic, $methodName)) {
                call_user_func_array(array($reply, $methodName), array(trim($request->getParameter($name))));
            }
        }
    }
    
    private function getLoginId() {
        return $this->getUser()->getAttribute('login')->getId();
    }
}

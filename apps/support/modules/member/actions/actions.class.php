<?php
class memberActions extends sfActions {
    /**
     * Executes index action
     *
     * @param sfRequest $request
     *            A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->forward ( 'default', 'login' );
    }
    
    public function executeLogin( sfWebRequest $request ) {
        $user = new User();
        if( sfRequest::POST == $request->getMethod() ) {
            $this->bindUser($user, $request);
            if($errors = $this->validateLogin($user)) {
                $this->errors = $errors;
            }
            else {
                
                $login = UserQuery::create()
                    ->filterByEmail(trim($request->getParameter('email')))
                    ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
                    ->findOne();
                if($login && password_verify($request->getParameter('password'), $login->getPassword())) {
                    $levels = RoleQuery::create()
                        ->filterByUserId($login->getId())
                        ->select('UserLevelCode')
                        ->find()->toArray();
                    
                    $this->getUser()->setAttribute('login', $login);
                    $this->getUser()->setAttribute('role', $levels);
                    
                    $this->redirect('@topic');
                }
                else {
                    
                    $this->errors = array('message' => 'Invalid email or password!');
                }
            }
        }
        
        $this->user = $user;
    }
    
    public function executeLogout(sfWebRequest $request) {
        $holder = $this->getUser()->getAttributeHolder();
        $holder->remove('login');
        $holder->remove('role');
        
        $this->redirect('@login');
    }
    
    public function executeRegister( sfWebRequest $request ) {
        $user = new User();
        if($request->getMethod() == sfRequest::POST) {
            $this->bindUser($user, $request);
            if($errors = $this->validateRegister($user)) {
                $this->user = $user;
                $this->errors = $errors;
            }
            else {
                $user
                    ->setPassword(password_hash($user->getPassword(), PASSWORD_DEFAULT))
                    ->setAuditFlag('C')
                    ->setCreatedAt(time())
                    ->save();
                $this->getUser()->setFlash('status', 'Register completed');
                $this->redirect('@login');
            }
        }
        
        $this->user = $user;
    }
    
    private function validateLogin(User $user) {
        $errors = array();
        if(!$user->getEmail()) {
            $errors['email'] = 'This field is required!';
        }
        
        if(!$user->getPassword()) {
            $errors['password'] = 'This field is required!';
        }
        
        return $errors;
    }
    
    private function validateRegister(User $user) {
        $errors = array();
        if(!$user->getEmail()) {
            $errors['email'] = 'This field is required!';
        } else {
            $old = UserQuery::create()
                ->filterByEmail($user->getEmail())
                ->filterbyAuditFlag('D', Criteria::NOT_EQUAL)
                ->findOne();
            if($old) {
                $errors['email'] = 'This email is exists!';
            }
        }
        
        return $errors;
    }
    
    private function bindUser(User $user, sfWebRequest $request ) {
        $names = $request->getParameterHolder()->getNames();
        foreach($names as $name) {
            $methodName = 'set' . ucfirst($name);
            if(method_exists($user, $methodName)) {
                call_user_func_array(array($user, $methodName), array(trim($request->getParameter($name))));
            }
        }
    }
}

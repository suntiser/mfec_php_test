<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <?php include_http_metas()?>
    <?php include_metas()?>
    <?php include_title()?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets()?>
    <script type="text/javascript" src="<?php echo _compute_public_path( 'jquery.js', 'js', 'js', true)?>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <title>Support</title>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo sfConfig::get('app_base_url')?>/js/html5shiv.js"></script>
      <script src="<?php echo sfConfig::get('app_base_url')?>/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <form action="<?php echo url_for('@register')?>" method="post">
        <table>
            <tr>
                <td>Email</td>
                <td>
                    <input type="text" name="email" value="<?php echo $user->getEmail()?>" />
                    <?php if(isset($errors['email'])):?>
                        <div class="error-text"><?php echo $errors['email']?></div>
                    <?php endif;?>    
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <input type="password" name="password" />
                    <?php if(isset($errors['password'])):?>
                        <div class="error-text"><?php echo $errors['password']?></div>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td>
                    <input type="password" name="password" />
                    <?php if(isset($errors['password'])):?>
                        <div class="error-text"><?php echo $errors['password']?></div>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <td>Firstname</td>
                <td>
                    <input type="text" name="firstName" value="<?php echo $user->getFirstName()?>" />
                    <?php if(isset($errors['firstName'])):?>
                        <div class="error-text"><?php echo $errors['firstName']?></div>
                    <?php endif;?>    
                </td>
            </tr>
            <tr>
                <td>Lastname</td>
                <td>
                    <input type="text" name="lastName" value="<?php echo $user->getLastName()?>" />
                    <?php if(isset($errors['lastName'])):?>
                        <div class="error-text"><?php echo $errors['lastName']?></div>
                    <?php endif;?>    
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Submit" />
                </td>
                <td>
                    <input type="button" value="Cancel" onclick="document.location='<?php echo url_for('@login')?>'"/>
                </td>
            </tr>
        </table>
    </form>
  </body>
</html>

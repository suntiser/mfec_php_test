<ul class="sidebar-menu" id="nav-accordion">
    <?php
    $login = $sf_user->getAttribute('login');
    $assignedGroups = kzRolesQuery::create()
        ->filterByUserId( $login->getId() )
        ->find();
    $assignedGroupsId = array();
    if( $assignedGroups ) {
        foreach ( $assignedGroups as $group ) {
            $assignedGroupsId[] = $group->getGroupId();
        }
    }
    ?>
    <?php foreach ( $programs as $program ):?>
        <?php
        $childs = kzProgramQuery::create()
            ->usekzProgramI18nQuery( 'i18n' )
                ->filterByLocale( 'en_EN' )
            ->endUse()->with( 'i18n' )
            ->usekzProgramGroupQuery( 'pg', Criteria::INNER_JOIN )
                ->filterByGroupId( $assignedGroupsId )
                ->filterByReadable( true )
            ->endUse()
            //->leftJoinkzProgramI18n( 'i18n' )->with('i18n')
            ->filterByParentId( $program->getId() )
            ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
            ->filterByDisplayAsMenu( true )
            ->orderByDisplayOrder( Criteria::ASC )
            ->find();?>
        <?php if( $childs && count( $childs ) > 0 ):?>
            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-sitemap"></i>
                    <span><?php echo $program->getName()?></span>
                </a>
                <ul class="sub">
                    <?php foreach( $childs as $child ):?>
                        <li class="sub-menu">
                            <a href="<?php echo $child->getUrl()? url_for( $child->getUrl() ): '#'?>">
                                <i class="fa fa-laptop"></i>
                                <span><?php echo $child->getName()?></span>
                            </a>
                        </li>
                    <?php endforeach;?>
                </ul>
            </li>
        <?php else:?>
            <li>
                <a href="<?php echo $program->getUrl()? url_for( $program->getUrl() ): '#'?>">
                    <i class="fa fa-laptop"></i>
                    <span><?php echo $program->getName()?></span>
                </a>
            </li>
        <?php endif;?>
    <?php endforeach;?>
</ul>
<a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="username">Selected branch: <?php echo $sf_user->getAttribute('selectedBranchName')?></span>
    <b class="caret"></b>
</a>
<ul class="dropdown-menu extended">
    <div class="log-arrow-up"></div>
    <?php foreach( $branches as $branch ):?>
        <li><a href="<?php echo url_for( "user/changeBranch?branchId={$branch->getId()}" )?>"><i class="fa fa-sitemap"></i>&nbsp;<?php echo $branch->getName()?></a></li>
    <?php endforeach;?>
</ul>
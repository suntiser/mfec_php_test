<?php

/**
 * default actions.
 *
 * @package EABC
 * @subpackage default
 * @author Sunti Sireannoi
 * @version SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class defaultComponents extends sfComponents {
    /**
     * Executes index action
     *
     * @param sfRequest $request
     *            A request object
     */
    public function executeMenu( sfWebRequest $request ) {

        $login = $this->getUser()->getAttribute( 'login' );
        $assignedGroups = kzRolesQuery::create()
            ->filterByUserId( $login->getId() )
            ->find();
        $assignedGroupsId = array();

        if( $assignedGroups ) {
            foreach ( $assignedGroups as $group ) {
                $assignedGroupsId[] = $group->getGroupId();
            }
        }

        $this->programs = kzProgramQuery::create()
            ->usekzProgramGroupQuery()
                ->filterBykzGroup( $login->getGroups() )
                ->filterByReadable( true )
            ->endUse()
            ->joinWithI18n( 'en_EN', Criteria::LEFT_JOIN )
            ->filterByDisplayAsMenu( true )
            ->filterByAuditFlag( 'D', Criteria::NOT_EQUAL )
            ->filterByParentId( null )
            ->orderByDisplayOrder()
            ->find();
    }

    public function executeSelectingBranches( sfWebRequest $request ) {
        $this->branches = BranchQuery::create()
            ->useBranchI18nQuery( 'i18n', Criteria::LEFT_JOIN )
                ->filterByLocale( $this->getUser()->getCulture() )
                ->orderByName()
            ->endUse()
            ->with( 'i18n' )
            ->useBranchAssignmentQuery()
                ->filterByUserId( $this->getUser()->getAttribute( 'login' )->getId() )
            ->endUse()
            ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
            ->find();
    }
}

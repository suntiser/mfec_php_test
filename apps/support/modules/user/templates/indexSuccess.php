<table width="600px">
    <tr>
        <th>#</th>
        <th>Subject</th>
    </tr>
    <?php foreach ($users as $i => $user):?>
        <tr>
            <td><?php echo $i + 1?></td>
            <td>
                <?php echo htmlspecialchars($user->getEmail())?>
            </td>
            <td>
                <?php $roles = $user->getRoles()->toKeyValue('UserLevelCode','UserId')?>
                <?php if(array_key_exists('ADM', $roles)):?>
                    <a href="<?php echo url_for("user/removeRole?level=ADM&userId={$user->getId()}")?>">Remove from Administrator</a>
                <?php else:?>
                    <a href="<?php echo url_for("user/addRole?level=ADM&userId={$user->getId()}")?>">Add to Administrator</a>
                <?php endif;?>
                <?php if(array_key_exists('MOD', $roles)):?>
                    | <a href="<?php echo url_for("user/removeRole?level=MOD&userId={$user->getId()}")?>">Remove from Moderator</a>
                <?php else:?>
                    | <a href="<?php echo url_for("user/addRole?level=MOD&userId={$user->getId()}")?>">Add to Moderator</a>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
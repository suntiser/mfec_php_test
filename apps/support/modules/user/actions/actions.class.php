<?php
class userActions extends sfActions {
    /**
     * Executes index action
     *
     * @param sfRequest $request
     *            A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->users = UserQuery::create()
            ->filterByAuditFlag('D', Criteria::NOT_EQUAL)
            ->orderByEmail()
            ->find();
    }
    
    public function executeDelete(sfWebRequest $request) {
        UserQuery::create()
            ->filterById($request->getParameter('id'))
            ->update(array('AuditFlag' => 'D', 'UpdatedAt' => time(), 'UpdatedById' => $this->getLoginId()));
        $this->redirect('user/index');
    }
    
    public function executeAddRole(sfWebRequest $request) {
        $role = new Role();
        $role
            ->setUserId($request->getParameter('userId'))
            ->setUserLevelCode($request->getParameter('level'))
            ->save();
        $this->redirect('user/index');
    }
    
    public function executeRemoveRole(sfWebRequest $request) {
        RoleQuery::create()
            ->filterByUserId($request->getParameter('userId'))
            ->filterByUserLevelCode($request->getParameter('level'))
            ->delete();
        $this->redirect('user/index');
    }
    
    private function getLoginId() {
        return $this->getUser()->getAttribute('login')->getId();
    }
}

<?php $uri = $_SERVER['REQUEST_URI']?>
<?php $uri = str_replace( ':443', '', $uri )?>
<?php $uri = preg_replace( '/\&page\=[0-9]*/i', '', $uri )?>
<?php $sign = strpos( $uri, '?' ) > 0? '&': '?'?>
<div class="panel-body">
    <div class="span6">
        <div class="dataTables_info" id="hidden-table-info_info">
            Total <?php echo $contents->getNbResults()?> entries
        </div>
    </div>
    <?php if( isset( $contents ) && count( $contents ) > $pagingOptions['maxPerPage'] ):?>
        <div class="span6">
            <div class="dataTables_paginate paging_bootstrap pagination">
                <ul>
                    <?php if( $contents->isFirstPage() ):?>
                        <li class="prev disabled"><a href="javascript:void(0)">← Previous</a></li>
                    <?php else:?>
                        <li class="prev"><a href="<?php echo $uri . $sign?>page=<?php echo $contents->getPage() - 1?>">← Previous</a></li>
                    <?php endif;?>
                    
                    <?php $links = $contents->getLinks();?>
                    <?php foreach( $links as $link ):?>
                        <li class="<?php echo $link == $contents->getPage()? 'active': ''?>"><a href="<?php echo $uri . $sign?>page=<?php echo $link?>"><?php echo $link?></a></li>
                    <?php endforeach;?>
                    <?php if( $contents->isLastPage() ):?>
                        <li class="next disabled"><a href="#">Next → </a></li>
                    <?php else:?>
                        <li class="next"><a href="<?php echo $uri . $sign?>page=<?php echo $contents->getPage() + 1?>">Next → </a></li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    <?php endif;?>
</div>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <?php include_http_metas()?>
    <?php include_metas()?>
    <?php include_title()?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets()?>
    <script type="text/javascript" src="<?php echo _compute_public_path( 'jquery.js', 'js', 'js', true)?>"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="img/favicon.png" />
    <title>Support</title>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo sfConfig::get('app_base_url')?>/js/html5shiv.js"></script>
      <script src="<?php echo sfConfig::get('app_base_url')?>/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .error-text{color:red}
    </style>
  </head>
  <body style="padding: 15px;">
    <a href="<?php echo url_for('@topic')?>">Topics</a>
    <?php if(Role::isAdministrator($sf_user->getAttribute('role'))):?>
        | <a href="<?php echo url_for('user/index')?>">Users</a>
    <?php endif;?>
    | <a href="<?php echo url_for('member/logout')?>">Logout</a>
    <?php echo $sf_content?>
  </body>
</html>

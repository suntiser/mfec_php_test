<?php
class CMSProductService {

    public static $CATEGORY_BOOTS        = 1;
    public static $CATEGORY_KIT          = 2;
    public static $CATEGORY_EQUIPMENT    = 3;
    public static $CATEGORY_APPAREL      = 4;
    public static $CATEGORY_ARI_GEAR     = 5;
}
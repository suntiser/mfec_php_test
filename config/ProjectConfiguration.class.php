<?php
require_once dirname ( __FILE__ ) . '/../lib/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register ();
class ProjectConfiguration extends sfProjectConfiguration {
    public function setup() {
        sfConfig::set ( 'sf_propel_path', dirname ( __FILE__ ) . '/../lib/propel' );
        $this->enablePlugins ( 'sfPropelORMPlugin' );
    }
}

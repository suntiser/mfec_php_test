<?php


/**
 * Base class that represents a query for the 'tbl_topic' table.
 *
 *
 *
 * @method TopicQuery orderById($order = Criteria::ASC) Order by the id column
 * @method TopicQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method TopicQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method TopicQuery orderByAuditFlag($order = Criteria::ASC) Order by the audit_flag column
 * @method TopicQuery orderByCreatedById($order = Criteria::ASC) Order by the created_by_id column
 * @method TopicQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method TopicQuery orderByUpdatedById($order = Criteria::ASC) Order by the updated_by_id column
 * @method TopicQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method TopicQuery groupById() Group by the id column
 * @method TopicQuery groupByTitle() Group by the title column
 * @method TopicQuery groupByMessage() Group by the message column
 * @method TopicQuery groupByAuditFlag() Group by the audit_flag column
 * @method TopicQuery groupByCreatedById() Group by the created_by_id column
 * @method TopicQuery groupByCreatedAt() Group by the created_at column
 * @method TopicQuery groupByUpdatedById() Group by the updated_by_id column
 * @method TopicQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method TopicQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method TopicQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method TopicQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method TopicQuery leftJoinCreatedBy($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedBy relation
 * @method TopicQuery rightJoinCreatedBy($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedBy relation
 * @method TopicQuery innerJoinCreatedBy($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedBy relation
 *
 * @method TopicQuery leftJoinReply($relationAlias = null) Adds a LEFT JOIN clause to the query using the Reply relation
 * @method TopicQuery rightJoinReply($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Reply relation
 * @method TopicQuery innerJoinReply($relationAlias = null) Adds a INNER JOIN clause to the query using the Reply relation
 *
 * @method Topic findOne(PropelPDO $con = null) Return the first Topic matching the query
 * @method Topic findOneOrCreate(PropelPDO $con = null) Return the first Topic matching the query, or a new Topic object populated from the query conditions when no match is found
 *
 * @method Topic findOneByTitle(string $title) Return the first Topic filtered by the title column
 * @method Topic findOneByMessage(string $message) Return the first Topic filtered by the message column
 * @method Topic findOneByAuditFlag(string $audit_flag) Return the first Topic filtered by the audit_flag column
 * @method Topic findOneByCreatedById(int $created_by_id) Return the first Topic filtered by the created_by_id column
 * @method Topic findOneByCreatedAt(string $created_at) Return the first Topic filtered by the created_at column
 * @method Topic findOneByUpdatedById(int $updated_by_id) Return the first Topic filtered by the updated_by_id column
 * @method Topic findOneByUpdatedAt(string $updated_at) Return the first Topic filtered by the updated_at column
 *
 * @method array findById(int $id) Return Topic objects filtered by the id column
 * @method array findByTitle(string $title) Return Topic objects filtered by the title column
 * @method array findByMessage(string $message) Return Topic objects filtered by the message column
 * @method array findByAuditFlag(string $audit_flag) Return Topic objects filtered by the audit_flag column
 * @method array findByCreatedById(int $created_by_id) Return Topic objects filtered by the created_by_id column
 * @method array findByCreatedAt(string $created_at) Return Topic objects filtered by the created_at column
 * @method array findByUpdatedById(int $updated_by_id) Return Topic objects filtered by the updated_by_id column
 * @method array findByUpdatedAt(string $updated_at) Return Topic objects filtered by the updated_at column
 *
 * @package    propel.generator.support.om
 */
abstract class BaseTopicQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseTopicQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'propel';
        }
        if (null === $modelName) {
            $modelName = 'Topic';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new TopicQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   TopicQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return TopicQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof TopicQuery) {
            return $criteria;
        }
        $query = new TopicQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Topic|Topic[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = TopicPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(TopicPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Topic A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Topic A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `message`, `audit_flag`, `created_by_id`, `created_at`, `updated_by_id`, `updated_at` FROM `tbl_topic` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Topic();
            $obj->hydrate($row);
            TopicPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Topic|Topic[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Topic[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TopicPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TopicPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TopicPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TopicPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TopicPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TopicPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $message)) {
                $message = str_replace('*', '%', $message);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TopicPeer::MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the audit_flag column
     *
     * Example usage:
     * <code>
     * $query->filterByAuditFlag('fooValue');   // WHERE audit_flag = 'fooValue'
     * $query->filterByAuditFlag('%fooValue%'); // WHERE audit_flag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $auditFlag The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByAuditFlag($auditFlag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($auditFlag)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $auditFlag)) {
                $auditFlag = str_replace('*', '%', $auditFlag);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(TopicPeer::AUDIT_FLAG, $auditFlag, $comparison);
    }

    /**
     * Filter the query on the created_by_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedById(1234); // WHERE created_by_id = 1234
     * $query->filterByCreatedById(array(12, 34)); // WHERE created_by_id IN (12, 34)
     * $query->filterByCreatedById(array('min' => 12)); // WHERE created_by_id >= 12
     * $query->filterByCreatedById(array('max' => 12)); // WHERE created_by_id <= 12
     * </code>
     *
     * @see       filterByCreatedBy()
     *
     * @param     mixed $createdById The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByCreatedById($createdById = null, $comparison = null)
    {
        if (is_array($createdById)) {
            $useMinMax = false;
            if (isset($createdById['min'])) {
                $this->addUsingAlias(TopicPeer::CREATED_BY_ID, $createdById['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdById['max'])) {
                $this->addUsingAlias(TopicPeer::CREATED_BY_ID, $createdById['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TopicPeer::CREATED_BY_ID, $createdById, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(TopicPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(TopicPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TopicPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_by_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedById(1234); // WHERE updated_by_id = 1234
     * $query->filterByUpdatedById(array(12, 34)); // WHERE updated_by_id IN (12, 34)
     * $query->filterByUpdatedById(array('min' => 12)); // WHERE updated_by_id >= 12
     * $query->filterByUpdatedById(array('max' => 12)); // WHERE updated_by_id <= 12
     * </code>
     *
     * @param     mixed $updatedById The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByUpdatedById($updatedById = null, $comparison = null)
    {
        if (is_array($updatedById)) {
            $useMinMax = false;
            if (isset($updatedById['min'])) {
                $this->addUsingAlias(TopicPeer::UPDATED_BY_ID, $updatedById['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedById['max'])) {
                $this->addUsingAlias(TopicPeer::UPDATED_BY_ID, $updatedById['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TopicPeer::UPDATED_BY_ID, $updatedById, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at < '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(TopicPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(TopicPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TopicPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TopicQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCreatedBy($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(TopicPeer::CREATED_BY_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TopicPeer::CREATED_BY_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedBy() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedBy relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function joinCreatedBy($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedBy');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedBy');
        }

        return $this;
    }

    /**
     * Use the CreatedBy relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedBy($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedBy', 'UserQuery');
    }

    /**
     * Filter the query by a related Reply object
     *
     * @param   Reply|PropelObjectCollection $reply  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 TopicQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByReply($reply, $comparison = null)
    {
        if ($reply instanceof Reply) {
            return $this
                ->addUsingAlias(TopicPeer::ID, $reply->getTopicId(), $comparison);
        } elseif ($reply instanceof PropelObjectCollection) {
            return $this
                ->useReplyQuery()
                ->filterByPrimaryKeys($reply->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByReply() only accepts arguments of type Reply or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Reply relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function joinReply($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Reply');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Reply');
        }

        return $this;
    }

    /**
     * Use the Reply relation Reply object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ReplyQuery A secondary query class using the current class as primary query
     */
    public function useReplyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinReply($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Reply', 'ReplyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Topic $topic Object to remove from the list of results
     *
     * @return TopicQuery The current query, for fluid interface
     */
    public function prune($topic = null)
    {
        if ($topic) {
            $this->addUsingAlias(TopicPeer::ID, $topic->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}

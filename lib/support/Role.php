<?php

/**
 * Skeleton subclass for representing a row from the 'tbl_role' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements. This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package propel.generator.support
 */
class Role extends BaseRole {
    
    public static function allowDelete($roles) {
        return array_search('ADM', $roles) !== false || array_search('MOD', $roles) !== false;
    }
    
    public static function isAdministrator($roles) {
        return array_search('ADM', $roles) !== false;
    }
}

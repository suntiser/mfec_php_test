<?php



/**
 * Skeleton subclass for representing a row from the 'tbl_user_level' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.support
 */
class UserLevel extends BaseUserLevel
{
}

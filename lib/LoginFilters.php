<?php
class LoginFilters extends sfFilter {
    
    public function execute (sfFilterChain $filterChain) {
        
        $context = $this->getContext();
        $request = $context->getRequest();
        $user = $context->getUser();
        $logger = sfContext::getInstance()->getLogger();
        
        
        if( $request->getParameter('action') == 'register' ) {
            return $filterChain->execute();
        }
        
        $login = $user->getAttribute('login');
        if( !isset( $login ) && ( $request->getParameter('module') != 'member' || ( $request->getParameter('module') == 'member' && $request->getParameter('action') != 'login' ) ) ) {
            sfContext::getInstance()->getLogger()->alert( "##### required login #######" );
            return $context->getController()->redirect('@login');
        }
        else {
            sfContext::getInstance()->getLogger()->alert( "##### Already logged in #######" );
            return $filterChain->execute();
        }
    }
}
<?php
class ApiUtil{
    
    public static function callJSONApi( $url, $headers = array(), $method = "GET", $data = array() ) {
        
        $jsonData = json_encode( $data, JSON_PRETTY_PRINT );
        sfContext::getInstance()->getLogger()->alert( $jsonData );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch, CURLOPT_POST, strtoupper($method) === "POST" );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    }
    
    public static function callStoreApi( $serviceName, $data, $method = 'GET', $params = '' ) {
        $line = str_repeat( '-' , 100 ) . "\n";
        $endpoint = 'https://api.eflowsys.com:8083/EC_API/{serviceName}?userAccount=Web&userPassword=WebReQ007pS&compCode=ari&testMode=1';
        $url = str_replace( '{serviceName}', $serviceName, $endpoint ) . $params;
        $response = static::callJSONApi( $url, array(), $method, $data );
        
        
        $fileName = sfConfig::get( 'app_api_output_dir' ) . '/' . $serviceName . '-' . date('YmdHis-u').'.txt';
        $content = $line . "Url: $url\n$line Parameters: \n$line" . json_encode( $data, JSON_PRETTY_PRINT ) . "\n$line Output\n$line";
        
        $jsonResponse = json_encode( json_decode( $response ), JSON_PRETTY_PRINT );
        
        sfContext::getInstance()->getLogger()->alert( $content.$jsonResponse );
        file_put_contents( $fileName, $content.$jsonResponse );
        
        return $response;
    }
}


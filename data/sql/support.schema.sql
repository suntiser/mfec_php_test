
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- tbl_user_level
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user_level`;

CREATE TABLE `tbl_user_level`
(
    `code` VARCHAR(25) NOT NULL,
    `name` VARCHAR(25) NOT NULL,
    PRIMARY KEY (`code`,`name`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tbl_user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(255),
    `password` VARCHAR(100),
    `first_name` VARCHAR(100),
    `last_name` VARCHAR(100),
    `audit_flag` VARCHAR(1),
    `created_by_id` INTEGER,
    `created_at` DATETIME,
    `updated_by_id` INTEGER,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tbl_role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role`
(
    `user_id` INTEGER NOT NULL,
    `user_level_code` VARCHAR(25) NOT NULL,
    PRIMARY KEY (`user_id`,`user_level_code`),
    INDEX `tbl_role_FI_1` (`user_level_code`),
    CONSTRAINT `tbl_role_FK_1`
        FOREIGN KEY (`user_level_code`)
        REFERENCES `tbl_user_level` (`code`),
    CONSTRAINT `tbl_role_FK_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tbl_topic
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_topic`;

CREATE TABLE `tbl_topic`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255),
    `message` TEXT,
    `audit_flag` VARCHAR(1),
    `created_by_id` INTEGER,
    `created_at` DATETIME,
    `updated_by_id` INTEGER,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `tbl_topic_FI_1` (`created_by_id`),
    CONSTRAINT `tbl_topic_FK_1`
        FOREIGN KEY (`created_by_id`)
        REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tbl_reply
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_reply`;

CREATE TABLE `tbl_reply`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `topic_id` INTEGER,
    `message` TEXT,
    `created_at` DATETIME,
    `created_by_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `tbl_reply_FI_1` (`created_by_id`),
    INDEX `tbl_reply_FI_2` (`topic_id`),
    CONSTRAINT `tbl_reply_FK_1`
        FOREIGN KEY (`created_by_id`)
        REFERENCES `tbl_user` (`id`),
    CONSTRAINT `tbl_reply_FK_2`
        FOREIGN KEY (`topic_id`)
        REFERENCES `tbl_topic` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
